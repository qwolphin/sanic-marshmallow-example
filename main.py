from sanic import Sanic, response
from accounts import bp as accounts_bp
from marshmallow.exceptions import ValidationError


app = Sanic('some_proxy')
app.blueprint(accounts_bp)

@app.exception(ValidationError)
async def validation_error_handler(request, exception):
    payload = exception.messages
    return response.json(payload, status=400)

if __name__ == '__main__':
  app.run(host='0.0.0.0', port=80)

