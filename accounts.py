from marshmallow import Schema, fields
from sanic.response import json, empty
from sanic import Blueprint
from sanic.views import HTTPMethodView


class ExtraFields(Schema):
    phone = fields.Str(required=True)


class AccountUpdateable(Schema):
    group = fields.Str(required=True, default='meow')
    extra_fields = fields.Nested(ExtraFields, missing=dict)


class Account(AccountUpdateable):
    login = fields.Integer(required=True)


accounts = {}


class AccountBase(HTTPMethodView):
    def some_helper(some_arg):
        pass


class AccountListCreate(AccountBase):
    async def get(self, request):
        data = Account().dump(accounts.values(), many=True)
        return json(data)

    async def post(self, request):
        account = Account().load(request.json)
        login = account['login']

        if login in accounts:
            return json('Login is used', status=409)

        accounts[login] = account
        return empty(status=204)


class AccountRetrieveUpdate(AccountBase):
    async def post(self, request, login):
        try:
            account = accounts[login]
        except KeyError:
            return empty(status=404)

        new_account = AccountUpdateable().load(request.json, partial=True)
        accounts[login] = {**account, **new_account}

        return empty(status=204)

    async def get(self, request, login):
        try:
            account = accounts[login]
        except KeyError:
            return empty(status=404)

        data = Account().dump(account)
        return json(data)


bp = Blueprint('accounts', url_prefix='/account')
bp.add_route(AccountListCreate.as_view(), '/')
bp.add_route(AccountRetrieveUpdate.as_view(), '/<login:int>')
